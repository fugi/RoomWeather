{
  inputs = {
    nixpkgs.url = "github:NixOS/nixpkgs/nixpkgs-unstable";
    flake-utils.url = "github:numtide/flake-utils";
    arduino-nix.url = "github:bouk/arduino-nix";
    arduino-indexes = {
      url = "github:bouk/arduino-indexes";
      flake = false;
    };
  };

  outputs = {
    self,
    nixpkgs,
    flake-utils,
    arduino-nix,
    arduino-indexes,
  }:
  (flake-utils.lib.eachDefaultSystem (system:
    let
      pkgs = import nixpkgs {
        inherit system;
        overlays = [
          arduino-nix.overlay
          (arduino-nix.mkArduinoLibraryOverlay (arduino-indexes + /index/library_index.json))
          (arduino-nix.mkArduinoPackageOverlay (arduino-indexes + /index/package_index.json))
          (arduino-nix.mkArduinoPackageOverlay (arduino-indexes + /index/package_esp8266com_index.json))
        ];
      };
    in rec {
      packages.arduino-cli = pkgs.wrapArduinoCLI {
        libraries = [
          (arduino-nix.latestVersion pkgs.arduinoLibraries."Adafruit Unified Sensor")
          (arduino-nix.latestVersion pkgs.arduinoLibraries."DHT sensor library")
        ];
        packages = with pkgs.arduinoPackages; [
          (platforms.esp8266.esp8266."3.1.2".overrideAttrs {
            postInstall = "touch $out/$dirName/cores/esp8266/CommonHFile.h";
          })
        ];
      };
      devShells.default = pkgs.mkShell {
        packages = with pkgs; [
          python3
          packages.arduino-cli
        ];
      };
    }
  ));
}
